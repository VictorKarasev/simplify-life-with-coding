# Задача по выводу числе в диапозоне с определенным шагом

## Вариант для JS
```js
const range = (
  start,
  end,
  step = 1,
) => {
  if (start > end) {
    return 'set valid range';
  }
  let range = [];
  while (start <= end) {
    range[range.length] = start;
    start += step;
  }
  return range;
}
```